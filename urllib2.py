import json
import urllib2
import requests
import ConfigParser
import os

# current path
cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

# read ini file
try:
    config = ConfigParser.ConfigParser()
    config.read(cur_dir + 'global.ini')
except IOError:
    print 'Failed to load ini file'

# url
URL_BASE = config.get('URL', 'URL_BASE')
URL_REG = URL_BASE + config.get('URL', 'URL_REG')
URL_AUTH = URL_BASE + config.get('URL', 'URL_AUTH')
URL_UPLOAD = URL_BASE + config.get('URL', 'URL_UPLOAD')
CIK_0001 = config.get('CIK', 'CIK_0001')
CIK_0004 = config.get('CIK', 'CIK_0004')


# http client class
class HTTPClient:
    sn = ''
    cik = ''
    logger = None

    def __init__(self, sn, cik, logger):
        self.sn = sn
        self.cik = cik
        self.logger = logger

    @staticmethod
    def register(sn, name):
        """
        Register new router to cloud
        :param sn:
        :param name:
        :return:
        """
        try:
            payload = {'sn': sn, 'name': name}
            r = requests.put(URL_REG, data=payload)
            return r.json()
        except Exception as e:
            print('%s in register', e)

    def authenticate(self):
        """
        Authenticate with given `sn` and `cik` value
        :return: Type of JSON
        """
        try:
            data = {'sn': self.sn, 'cik': self.cik}
            req = urllib2.Request(URL_AUTH)
            req.add_header('Content-Type', 'application/json')
            response = urllib2.urlopen(req, json.dumps(data))
            result = json.loads(response.read())

            if result['code'] == 'OK':
                return True, 'OK'
            else:
                return False, result['description']
        except Exception as e:
            self.logger.error('%s in authenticate', e)
            return False, e

    def get_request_from_cloud(self, request_type):
        """
        Get request form cloud server
        :param request_type: CMD, SETPOINT, STREAMINFO
        :return: JSON Text
        """
        try:
            url_request = URL_BASE + config.get('URL', 'URL_GET_' + request_type)
            url = url_request.replace('sn', self.sn)
            req = urllib2.Request(url)
            req.add_header('cik', self.cik)
            resp = urllib2.urlopen(req)
            content = json.loads(resp.read())

            if content['code'] == 'OK':
                return True, content['response']
            else:
                return False, content['description']
        except Exception as e:
            self.logger.error('%s in get_%s_from_cloud', e, request_type)
            return False, e

    def upload_to_cloud(self, state):
        """
        Upload current state of router to cloud server
        :param state: Current state of router in format of JSON
        :return:
        """
        try:
            headers = {'cik': self.cik, 'Content-Type': 'application/json'}
            url = URL_UPLOAD.replace('sn', self.sn)
            payload = json.dumps(state, ensure_ascii=False).encode('utf-8')
            self.logger.debug('Device Status: %s\r\n\r\n', payload)
            r = requests.put(url, data=payload, headers=headers)
            return r.json()
        except Exception as e:
            self.logger.error('%s in upload_to_cloud', e)


if __name__ == '__main__':
    http_client = HTTPClient(sn='0001', cik=config.get('CIK', 'CIK_0001'), logger=None)
    print http_client.authenticate()
    print http_client.get_request_from_cloud('CMD')
